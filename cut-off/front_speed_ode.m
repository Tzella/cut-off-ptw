clear all; close all;

val=[1e-11:1e-12:9e-11 1e-10:1e-11:9e-10 1e-9:1e-10:9e-9 1e-8:1e-9:9e-8 1e-7:1e-8:9e-7 1e-6:1e-7:9e-6 1e-5:1e-6:9e-5 1e-4:1e-5:9e-4 1e-3:1e-4:9e-3 1e-2:1e-3:9e-2 1e-1:1e-3:0.999];


tol1=1e-13;tol2=1e-13; epsilon=1e-12; 
    

opts = odeset('Reltol',tol1,'AbsTol',tol1,'Stats','off');

for i=1:1:length(val)
    uc=val(i);  

    if (i==1)
        
        v1=0; v2=2; %guess for speed
    else
        v1=0; v2=vp+0.1; %initial guesses
    
    end

    b1 =(v1-sqrt(4+v1^2))*epsilon/2;
    b2 =(v2-sqrt(4+v2^2))*epsilon/2;
    
    [a,b1] = ode45(@(a,b) fc_ode(a,b,v1),[1-epsilon uc],b1,opts);
    [a,b2] = ode45(@(a,b) fc_ode(a,b,v2),[1-epsilon uc],b2,opts);

    diff1=-v1*a(end)-b1(end);
    diff2=-v2*a(end)-b2(end);
    
    if diff1*diff2>0
        disp('Wrong choice bro')
    else
        vp = (v1 + v2)/2;
        bp =(vp-sqrt(4+vp^2))*epsilon/2;
        
        [a,bp] = ode45(@(a,b) fc_ode(a,b,vp),[1-epsilon uc],bp,opts);
        
        diffp=-vp*a(end)-bp(end);

        while (abs(v1-v2)>= tol2 || (abs(diff1)>= tol2 && abs(diff2)>= tol2))
            if diff1*diffp<0
                v2 = vp;
                b2 = bp;
                diff2 = diffp;
            else
                v1 = vp;
                b1 = bp;
                diff1 = diffp;
            end
            vp = (v1 + v2)/2;
            
            bp =(vp-sqrt(4+vp^2))*epsilon/2;
            [a,bp] = ode45(@(a,b) fc_ode(a,b,vp),[1-epsilon uc],bp,opts);

            diffp=-vp*a(end)-bp(end);
     
        end
    end
    
    disp([uc vp i/length(val)*100]);
   
    data(i).epsilon=epsilon; %associated with solution near alpha=1
    data(i).tol2=tol2;
    data(i).tol1=tol1;
    data(i).uc=uc;
    data(i).v=vp;
    data(i).a=a;
    data(i).b=bp;
end
% 
 speed_ode.data=data;
 save('speed_ode', '-struct', 'speed_ode');
