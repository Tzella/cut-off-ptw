function dbda = fc_ode(a,b,v)

if b~=0
    dbda = -v-a*(1-a)/b;
end

end  
