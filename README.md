# cut-off PTW

Code for producing results for the paper:
The Evolution of Travelling Waves in a KPP Reaction-Diffusion Model with cut-off Reaction Rate. I. Permanent Form Travelling Waves. 

Alex D. O. Tisbury, David J. Needham and Alexandra Tzella  

There are three directories:

no-cut-off focuses on the Fisher KPP reaction and determines the constants in the leading edge behaviour of the front

cut-off focuses on the cut-off Fisher KPP reaction and determines the speed and profile of the corresponding front

evolution focuses on the cut-off Fisher KPP reaction and determines the evolution of the front as obtained from the solution to the QIVP
