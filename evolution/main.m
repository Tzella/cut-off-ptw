% this solves the fixed boundary reaction-diffusion equation u_t = u_yy + sdot u_y +f(u) with heaviside initial 
% data and continuous 1st derivative in space at QL/QR interface  using a fully explicit discretization.  t_0 is the initial time, t_f is 
% the final time, J1+J2 is the number of mesh-points, and N is the number of
% time steps. 

%  cutoff
uc=0.1;

% time the program runs for
t_0 = 0;
t_f = 50;

% define space mesh limits
ymax = 25;
ymin = -10;

dy = 0.01;

% define ratio betweeen distance steps and size of time steps
r=0.4;
dt=dy^2*r; 

% define the mesh in QL and QR 
y1 = dy:dy:ymin;y1=-y1;y1=sort(y1);
y2 = dy:dy:ymax;
y = [y1 y2];
y = y';

J1=length(y1);J2=length(y2);

% define the mesh in time
dt=round(10e10*dt)/10e10;
t = t_0:dt:t_f;
N=length(t);

% preallocating size of distance/velocity vectors
s = zeros(1,N); 
sdot = zeros(1,N);
err=s;  
u = zeros(J1+J2+1,1);


% initial values of u
%front-like initial conditions
h= y<0; u(h,1)=1;
h= y>0; u(h,1)=0;

U(:,1)=u;

k=2;
%time loop
for n=1:N
    
    % tracking where the front would be in moving bounadry problem at each
    % time step
    s(n)=u(J1+J2+1);
    
    % use forward differences for time derivates
    % and explicit centred differences for space derivatives to obtain the matrices
    % A, B.  
    [A,B,d] = matrix(J1,J2,r,dy,dt,u,uc);

    % Solving Aup=Bu+d - note final element in u,up corresponds to s at
    % respective time steps i.e. s(n)=u(2*J+1), s(n+1)=up(2*J+1)
    up =  A\(B*u+d);
    sdot(n)=(up(J1+J2+1)-u(J1+J2+1))/dt;
        
    % updating values of u at each time step
    u=up;
    
end
