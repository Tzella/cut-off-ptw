 
function [A,B,d] = matrix(J1,J2,r,dy,dt,u,uc)

% for ease of notation
nu=-1/(2*dy);

% note - elements in column J1+J2+1 of matrices correspond to the coefficients of sdot
% as u(J1+J2+1) corresponds to sdot at each time step. 

f=zeros(J1+J2,1);
f(1) = nu*(u(2)-1);
for i=2:J1+J2-1
    f(i)=nu*(u(i+1)-u(i-1));
end
f(J1)=2*nu*(uc-u(J1-1));
f(J1+1) = 2*nu*(u(J1+2)-uc);
f(J1+J2) = -nu*u(J1+J2-1);

%% Sparse Matrix A
a1=ones(1,2*(J1+J2+1)); a2=a1; a3=a1;
for k=1:J1+J2
    a1(k)=k; a2(k)=k;
    a1(J1+J2+2+k)=k; a2(J1+J2+2+k)=J1+J2+1; a3(J1+J2+2+k)=f(k); 
end
a1(J1+J2+1)=J1+J2+1; a2(J1+J2+1)=J1; a1(J1+J2+2)=J1+J2+1; a2(J1+J2+2)=J1+1;  
A=sparse(a1,a2,a3);

%% Sparse Matrix B
% defining vectors that make up diagonals of matrix

b3a=(1-2*r)*ones(J1+J2,1); 
for i=1:J1
    b3a(i)=b3a(i) + dt*(1-u(i));
end
b3b=r*ones(2*(J1+J2-2),1); 
b3=[b3a; b3b; f];

b1=[1:J1+J2 2:J1 J1+2:J1+J2 1:J1-1 J1+1:J1+J2-1 1:J1+J2]';
b2=[1:J1+J2 1:J1-1 J1+1:J1+J2-1 2:J1 J1+2:J1+J2 (J1+J2+1)*ones(1,J1+J2)]';
B=sparse(b1,b2,b3);
B=[B; zeros(1,J1+J2+1)];

% vector d including boundary conditions
d=zeros(J1+J2+1,1); 

d(1)=r; 
d(J1)=r*uc;  
d(J1+1)=r*uc;  

%continuity of derivative condition is applied at j=J1+J2+1
d(J1+J2+1)=2*uc;


end

