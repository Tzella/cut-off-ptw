clear all; close all

epsilon=1e-12; v=2; %associated with solution near alpha=0
b = (v-sqrt(4+v^2))*epsilon/2;

opts = odeset('Reltol',1e-13,'AbsTol',1e-12,'Stats','on');
opts = odeset('Reltol',1e-13,'AbsTol',1e-13,'Stats','on');
[t,y] = ode45(@f_bwd,[0 10],[1-epsilon; b],opts);

a=y(:,1);
b=y(:,2);

[t,y] = ode45(@f,[0 10000],[1-epsilon; b(1)],opts);

a=y(:,1);
b=y(:,2);

figure(1); plot(a,b)

h=find(a<1/2,1,'first'); %zero order interpolation 
 
t=t-(t(h)+(0.5-a(h))*(t(h-1)-t(h))/(a(h-1)-a(h)));  %linear interpolation 

H=find(a<0.001  & a>1e-12); 
A=a(H);T=t(H);C=A./exp(-T);

figure(2);plot(T,A./exp(-T),'k');

xdata=T; ydata=C; 
P = polyfit(xdata,ydata,1);
yfit = P(1)*xdata+P(2);

hold on;plot(xdata,yfit,'k--')
 