function dydt = f_bwd(t,y)

dydt = [-y(2); 2*y(2)+y(1)*(1-y(1))];